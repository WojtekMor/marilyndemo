import gulp from 'gulp';
import gulpLoadPlugins from 'gulp-load-plugins';
import browserSync from 'browser-sync';
import del from 'del';
import runSequence from 'run-sequence';
import pipeErrorStop from 'pipe-error-stop';
import config from './gulp-config.json'

const $ = gulpLoadPlugins();
const reload = browserSync.reload;

const paths = {

	src: 'src',
	tmp: 'tmp',
	build: 'build',
  bower: 'bower_components'

};

let onError = (err) => {
  console.log(err.message);
  this.emit('end');
};

gulp.task('clean', del.bind(null, ['tmp', 'build']));

gulp.task('copyfonts', () => {
        return gulp.src([`${paths.bower}/**/*.{eot,ttf,woff,woff2,otf,svg}`])
              .pipe($.copy(`${paths.tmp}/fonts`, {prefix: 3}));
});

gulp.task('copyfonts:build', () => {
        return gulp.src([`${paths.bower}/**/*.{eot,ttf,woff,woff2,otf,svg}`])
              .pipe($.copy(`${paths.build}/fonts`, {prefix: 3}));
});

gulp.task('jade', () => {

  gulp.src([
      `${paths.src}/jade/**/*.jade`,
      `!${paths.src}/jade/**/_*.jade`
      ])
      .pipe($.plumber())
      .pipe($.jade())
      //.pipe(pipeErrorStop())
      .pipe(gulp.dest(paths.tmp))
      .pipe(reload({stream: true}));

});

gulp.task('jade:build', () => {

  gulp.src([
      `${paths.src}/jade/**/*.jade`,
      `!${paths.src}/jade/**/_*.jade`
      ])
      .pipe($.plumber())
      .pipe($.jade())
      //.pipe(pipeErrorStop())
      .pipe(gulp.dest(paths.build))
      .pipe(reload({stream: true}));

});

gulp.task(
    'imagemin', () => {

        gulp.src([
                `${paths.src}/img/**/*.{png,jpg,jpeg,gif,svg}`
            ])
            .pipe($.plumber({errorHandler: $.onError}))
            .pipe($.imagemin({
                optimizationLevel: 7,
                progressive: true
            }))
            .pipe(gulp.dest(`${paths.build}/img`));
    }
);

gulp.task('scripts:dev', () => {
  gulp.src(`${paths.src}/js/**/*.js`)
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.babel())
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest(`${paths.tmp}/scripts`))
    .pipe(reload({stream: true}));

   gulp.src(config.vendorjs)
    .pipe($.plumber())
    .pipe($.uglify())
    .pipe($.concat('vendors.js'))
    .pipe(gulp.dest(`${paths.tmp}/scripts`))
    .pipe(reload({stream: true}));

});

gulp.task('scripts:build', () => {
    
    gulp.src(`${paths.src}/js/**/*.js`)
      .pipe($.plumber())
      .pipe($.babel())
      .pipe(gulp.dest(`${paths.build}/scripts`))
      .pipe(reload({stream: true}));

    gulp.src(config.vendorjs)
      .pipe($.plumber())
      .pipe($.uglify())
      .pipe($.concat('vendors.js'))
      .pipe(gulp.dest(`${paths.build}/scripts`))
      .pipe(reload({stream: true}));

});

gulp.task('styles:dev', () => {
  return gulp.src([
    `${paths.src}/sass/**/*.sass`, 
    `!${paths.src}/sass/**/_*.sass`
    ])
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.sass.sync({
      outputStyle: 'expanded',
      precision: 10,
      includePaths: ['.']
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']}))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest(`${paths.tmp}/styles`))
    .pipe(reload({stream: true}));
});

gulp.task('styles:build', () => {
  return gulp.src([
    `${paths.src}/sass/**/*.sass`, 
    `!${paths.src}/sass/**/_*.sass`
    ])
    .pipe($.plumber())
    .pipe($.sass.sync({
      outputStyle: 'expanded',
      precision: 10,
      includePaths: ['.']
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']}))
    .pipe($.cssnano())
    .pipe(gulp.dest(`${paths.build}/styles`))

});

gulp.task('serve', () => {
  runSequence('clean', ['jade', 'copyfonts', 'styles:dev', 'scripts:dev']);
  browserSync({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['tmp', 'src'],
      routes: {
        '/bower_components': 'bower_components'
      }
    }
  });

  gulp.watch([
    'src/img/**/*',
    'src/fonts/**/*'
  ]).on('change', reload);

  gulp.watch('src/jade/**/*.jade', ['jade']);
  gulp.watch('src/sass/**/*.sass', ['styles:dev']);
  gulp.watch('src/js/**/*.js', ['scripts:dev']);
  gulp.watch('src/fonts/**/*', ['fonts']);
  //gulp.watch('bower.json', ['wiredep', 'fonts']);
});

gulp.task('serve:build', ['build'], () => {
  browserSync({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['build']
    }
  });
});

gulp.task('build', ()=> {
  runSequence('clean', ['jade:build', 'copyfonts:build', 'styles:build', 'scripts:build', 'imagemin']);
  });

gulp.task('default', () => {
  gulp.start('serve:build');
});