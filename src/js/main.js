let app = angular.module('app',['ngRoute']);

app.config(['$routeProvider',
  ($routeProvider) => {
    $routeProvider.
      when('/gallery', {
        templateUrl: './partials/gallery.html',
        controller: 'GalleryCtrl'
      }).
      when('/profile', {
        templateUrl: './partials/profile.html',
      }).
      otherwise({
        redirectTo: '/profile'
      });
  
  }]);

app.factory('flickrService', ($http) => {

    function getImages(tags){

        let url=`http://api.flickr.com/services/feeds/photos_public.gne?format=json&tags=${tags}&jsoncallback=JSON_CALLBACK`;
        
        return $http.jsonp(url);
    }

    return {getImages: getImages};

});

app.controller('NavbarCtrl', ($scope, $location)=> {

    $scope.isActive = (viewLocation) => {

     return viewLocation === $location.path()

    };

});

app.controller('GalleryCtrl', ($scope, flickrService) => {

    $scope.images = [];
    $scope.error = false;

    let imagesCount = 9;

    let thumbSuffixRegExp = new RegExp('_m.jpg$');
    let largeSuffix = '_h.jpg';
    let tags='marylin';

    function getLargeImageUrl(thumbUrl){

        return thumbUrl.replace(thumbSuffixRegExp, largeSuffix);

    };

    function processResponse(response){

        return response.data.items.slice(0, imagesCount).map((item) => {
            return {
                    'thumb': item.media.m,
                    'large': getLargeImageUrl(item.media.m)
                }

        });

    };

    function getImages() {

        flickrService.getImages(tags).then(
            (response) => {
                
                $scope.images = processResponse(response);

            },

            (response) => {
                
                $scope.error = true;
                console.log('Ooops, we have a problem');

            });

    };

    getImages();

});